class Assembly_Runner:
    def __init__(self):
        self.calc_codes = []
        self.regs = [None]*32
        self.read_file()
    def read_file(self):
        file = open("calc.txt",mode="r")
        self.calc_codes = file.read().splitlines()
    def run(self):
        for code in self.calc_codes:
            code_sections = code.split(" ")
            if code_sections[0] == "MOV":
                self.mov(code_sections[1],code_sections[2])
            elif code_sections[0] == "ADD":
                self.add(code_sections[1],code_sections[2])
            elif code_sections[0] == "SUB":
                self.sub(code_sections[1],code_sections[2])
            elif code_sections[0] == "DIV":
                self.div(code_sections[1],code_sections[2])
            elif code_sections[0] == "MUL":
                self.mul(code_sections[1],code_sections[2])
