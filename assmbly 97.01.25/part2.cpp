#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;
enum TokenType {NUMBER, PLUS, MINUS, MUL , DIV, LPAR, RPAR, EOF};
struct Token {TokenType type; int n; double r;  string s};
Token  t;  
bool debug_mod = true;
char buffer[200];
int tindex=0;
double E(void); 
double T(void); 
double F(void);
void debug(const char*,unsigned,const char*s="",double d=0);
void error(const char*);
void read(void);
void reset_all(void);
Token getToken(void){
  if(buffer[tindex] == '(')    t.type = LPAR;
  else if (buffer[tindex] == ')') t.type = RPAR;
  else if(buffer[tindex] == '+')  t.type = PLUS;
  else if(buffer[tindex] == '-')  t.type = MINUS;
  else if(buffer[tindex] == '*')  t.type = MUL;
  else if(buffer[tindex] == '/')  t.type = DIV;
  else if(buffer[tindex] >= '0'&&buffer[tindex] <='9')
  {
  	t.type=NUMBER;t.n=0;
    for(;buffer[tindex]>='0'&&buffer[tindex]<='9';tindex++) 		t.n=t.n*10+buffer[tindex]-48;
    tindex--;
  }
  else if(buffer[tindex] == '\0') 		t.type = EOF;
  else  error("unknown character");
  tindex++; //debug(__func__,__LINE__);
  return t;
}
void A(void){ 
	double x;
	t = getToken();  
	x= E();
    if(t.type != EOF) 		error("unrecognized token");
    cout<<"result is :"<<x<<endl;
}
double E(void){
	double x,y; 
	TokenType prev; 
	x=T();
    while(t.type==PLUS||t.type==MINUS)
	{
		debug(__func__,__LINE__);
  		prev=t.type; t=getToken();y=T();
    	if(prev == PLUS) x+= y;
    	calc = open('calc.txt','w+');
    	calc.write("MOV x,");
    	calc.write(x);
    	calc.write("MOV y,");
    	calc.write(y);
    	calc.write("ADD x,y");
    	else             x-=y;
    	calc = open('calc.txt','w+');
    	calc.write("MOV x,");
    	calc.write(x);
    	calc.write("MOV y,");
    	calc.write(y);
    	calc.write("SUB x,y");
    	debug(__func__,__LINE__,"After + or - :: ",x);
    } 	debug(__func__,__LINE__,"End E",x);  return x;
}
double T(void){
	double x,y; 
	TokenType prev;  
	x=F();
    while(t.type==MUL||t.type==DIV)
	{
		debug(__func__,__LINE__);
    	prev=t.type;t=getToken();y=F();
    	if(prev==MUL)   x*=y;
    	calc = open('calc.txt','w+');
    	calc.write("MOV x,");
    	calc.write(x);
    	calc.write("MOV y,");
    	calc.write(y);
    	calc.write("MUL x,y");
    	else            x/=y;
    	calc = open('calc.txt','w+');
    	calc.write("MOV x,");
    	calc.write(x);
    	calc.write("MOV y,");
    	calc.write(y);
    	calc.write("DIV x,y");
    	debug(__func__,__LINE__,"After * or / :: ",x);
    } 	debug(__func__,__LINE__,"End T",x); return x;
}
